#FROM maven:3-jdk-8-alpine
#WORKDIR /usr/src/app
#COPY . /usr/src/app
#RUN mvn package
#ENV PORT 5000
#EXPOSE $PORT
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]

FROM adoptopenjdk/openjdk8-openj9:jre8u222-b10_openj9-0.15.1-alpine
COPY app.jar .
EXPOSE 8080
ENV LANG en_GB.UTF-8
RUN apk update && apk add bash && apk add --update ttf-dejavu && rm -rf /var/cache/apk/* && apk add fontconfig
ENTRYPOINT java -jar /app.jar
